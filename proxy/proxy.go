package proxy // import "electric-it.io/cago/proxy"

import (
	"net"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/apex/log"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

func init() {
	RoundTripper := &http.Transport{
		Proxy: Proxy,
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
			DualStack: true,
		}).DialContext,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	}

	http.DefaultTransport = RoundTripper
}

// CanAccessTCPEndpoint checks to see whether an HTTP endpoint can be reached
func CanAccessTCPEndpoint(endpoint string) bool {
	if endpoint == "" {
		log.Debugf("Odd, I was asked to attempt to access a blank TCP endpoint")

		return false
	}

	connection, dialError := net.Dial("tcp", endpoint)
	if dialError != nil {
		log.Errorf("Error attempting to connect to endpoint (%s): ", endpoint, dialError)

		return false
	}

	closeError := connection.Close()
	if closeError != nil {
		log.Debugf("Error attempting to close connection to endpoint (%s): ", endpoint, closeError)

		return false
	}

	return true
}

// CanAccessHTTPEndpoint checks to see whether an HTTP endpoint can be reached
func CanAccessHTTPEndpoint(endpoint *url.URL) bool {
	if endpoint == nil {
		log.Debugf("Odd, I was asked to attempt to access a blank HTTP endpoint")

		return false
	}

	log.Debugf("Attempting HEAD to: %s", endpoint.Host)

	headResponse, headError := http.DefaultClient.Head(endpoint.String())
	if headError != nil {
		log.Errorf("Error calling HTTP HEAD on endpoint (%s): %+v", endpoint.String(), errors.WithStack(headError))

		return false
	}

	if headResponse.StatusCode != http.StatusOK {
		log.Errorf("Unexpected status returned for HTTP HEAD from endpoint (%s): %s", endpoint.String(), headResponse.Status)

		return false
	}

	log.Debugf("HEAD successful to: %s", endpoint.String())

	return true
}

func Proxy(request *http.Request) (*url.URL, error) {
	log.Debugf("Finding proxy for: %+v", request.URL)

	return getProxyURL(), nil
}

// getProxyURL returns the URL of the first proxy server that is working by discovering it in the following order:
//   1. Environment variable HTTP_PROXY
//   2. Environment variable http_proxy
func getProxyURL() *url.URL {
	// This will be set to the URL to use or remain nil
	var proxyURL *url.URL
	var parseError error

	// First, check the proxy environment variables
	proxyEnvironmentVariables := []string{"HTTP_PROXY", "http_proxy"}
	for _, proxyEnvironmentVariable := range proxyEnvironmentVariables {
		log.Debugf("Checking %s", proxyEnvironmentVariable)

		if proxyURLString, ok := os.LookupEnv(proxyEnvironmentVariable); ok {
			proxyURL, parseError = url.ParseRequestURI(proxyURLString)
			if parseError == nil {
				proxyHost := proxyURL.Host

				log.Debugf("Attempting to access proxy declared using %s endpoint: %s", proxyEnvironmentVariable, proxyHost)

				if CanAccessTCPEndpoint(proxyHost) {
					// Proxy URL parsed correctly and is accessible
					log.Debugf("Using proxy declared with %s environment variable: %s", proxyEnvironmentVariable, proxyHost)

					return proxyURL
				}

				log.Infof("Skipping proxy declared with %s environment variable as it is not reachable: %+v", proxyEnvironmentVariable, proxyURL)
			} else {
				log.Infof("Skipping proxy declared with %s environment variable, as parsing failed: %+v", proxyEnvironmentVariable, proxyURLString)
			}
		}
	}

	// Next, check to see if the user wants to ignore the proxies in the configuration file
	if viper.GetBool("ignore-proxy-config") {
		log.Info("Ignoring proxies in the configuration file because 'ignore-proxy-config' flag was set, no proxy will be used")

		return nil
	}

	// Grab the list of HTTP proxies from the configuration
	httpProxyStringMap := viper.GetStringMap("HTTPProxies")

	if len(httpProxyStringMap) == 0 {
		log.Infof("The HTTPProxies section of the configuration file does not contain any entries, no proxy will be used")

		return nil
	}

	log.Debugf("Checking (%v) proxies from the HTTPProxies section of the configuration file", len(httpProxyStringMap))

	// Try each proxy and use it if it's available
	for proxyAlias, httpProxy := range httpProxyStringMap {
		log.Debugf("Found proxy alias: %+v", proxyAlias)
		log.Debugf("httpProxy: %+v", httpProxy)

		proxyURLString := httpProxy.(map[string]interface{})["proxyurl"]
		if proxyURLString == nil {
			log.Warnf("The proxy entry %+v needs a ProxyURL key in the configuration file: %+v", proxyAlias, httpProxy)
			continue
		}

		proxyURL, parseError := url.ParseRequestURI(proxyURLString.(string))
		if parseError != nil {
			log.Debugf("Skipping proxy URL that couldn't be parsed: %+v", parseError)

			continue
		}

		if CanAccessTCPEndpoint(proxyURL.Host) {
			// Proxy URL parsed correctly and is accessible
			log.Debugf("Found good proxy from configuration file: %+v", proxyURL)

			return proxyURL
		}

	}

	log.Infof("Unable to find a working proxy from the configuration file")

	return nil
}
