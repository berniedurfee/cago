package cmd // import "electric-it.io/cago/cmd"

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"

	"electric-it.io/cago/aws"
)

// getAccessKeyIdCmd represents the getAccessKeyId command
var doesProfileExistCommand = &cobra.Command{
	Use:   "does-profile-exist",
	Short: "Prints '1' to stdout if the profile exists or '0' if it does not",
	Long:  `This command is useful for scripts that use Cago. It'll tell you whether or not a profile exists before trying to read or write the profile.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) != 1 {
			return errors.New("This command takes only a single argument, which is the profile name")
		}
		profileName := args[0]

		profileExists, doesProfileExistError := aws.DoesProfileExist(profileName)
		if doesProfileExistError != nil {
			return errors.Wrapf(doesProfileExistError, "Unable to determine if profile exists: %s", profileName)
		}

		// Print the result to stdout
		if profileExists {
			fmt.Println("1")
		} else {
			fmt.Println("0")

		}

		return nil
	},
}

func init() {
	rootCmd.AddCommand(doesProfileExistCommand)
}
